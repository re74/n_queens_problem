#include <iostream>

/*
* @desc Prints the board to the standard output. Board is represented as
    a 2D array of size `board_size` x `board_size`. 
    Zeroes represent empty cells. Ones represent queens.
* @param board_size The size of one side of the quadratic board.
* @param queens The array of placed queens where queens[i] holds the column position of the queen on the ith row.  
*/
void print_board (int board_size, int * queens)
{
    for (int r = 0; r < board_size; r++)
    {
        for (int c = 0; c < board_size; c++) 
            if (queens[r]==c)
                std::cout << 1 << " ";
            else 
                std::cout << 0 << " ";
        std::cout << std::endl;
    }
    std::cout << std::endl;
}

/*
* @desc Checks if the board is valid, i.e. the new queen wouldn't attack 
    the queens that already exist on the board. 
* @param n_placed_queens The number of placed queens, including the new(last) one.
* @param queens The array of placed queens where queens[i] holds the column position of the queen on the ith row.  
* @returns true if the board with the new queen is valid, and false otherwise  
*/
bool is_valid_board (int n_placed_queens, int* queens)
{

    for (int r = 0; r < n_placed_queens-1; r++)
    {
        // if there are any queens on the column or diagonals
        if (queens[r] == queens[n_placed_queens-1] || abs(n_placed_queens-1-r)==abs(queens[n_placed_queens-1]-queens[r]))
            return false;                 
    }
    return true;
}

/*
* @desc Solves the n-queens problem iteratively. 
* @param board_size The size of one side of the quadratic board.
* @queens The array of placed queens where queens[i] holds the column position of the queen on the ith row.
* @returns true if the problem is solved, and false otherwise   
*/
bool solve_n_queens_pr_iteratively (int board_size, int * queens)
{
    int n_placed_queens=0;
    int start_c=0; //column position to start search.
    //for each row
    for (int r = 0; r < board_size; r++)
    {
        //for possible column positions
        for (int c = start_c; c < board_size; c++)
        {
            //place queen
            queens[r]=c;
            n_placed_queens++;

            //if the board with the new queen is valid
            if (is_valid_board(n_placed_queens, queens))
                break;
            else 
            {
                //remove the queen
                queens[r]=-1;
                n_placed_queens--;  
            }                
        }
        //if didn't find place for the new queen
        if (n_placed_queens == r)
        {
            //if the new queen was the queen on the first row
            if (r == 0)
                return false;
            //correct the column position to start the search 
            //when replacing previous queen
            start_c = 1 + queens[r-1];
            //remove previous queen from its place
            queens[r-1]=-1;
            n_placed_queens--;
            //to replace previous queen on new iteration
            r-=2;            
        }
        else
            //to start from column 0 when placing the next queen
            start_c=0;
    }  
    return true;    
}

/* C++ program that receives an integer n in the range [1, 10], 
and prints out at least one solution (if any exist) 
to the problem of n-queen. 
(In this problem, n queens should be put on an n*n chess board
 such that no queen can attack any other.)
 The solution represents the board. Ones represent queens,
 and zeroes represent empty cells.
 If no solution exist, the corresponding message is presented.*/
 
int main()
{   
    int board_size = 0;
    //Get the board size
    while (!(board_size > 0 && board_size < 11))
    {
        std::cout << "Enter a board size in range 1 to 10 (inclusive): ";
        std::cin >> board_size;
    }        
    //Create the queens array
    int * queens = new int[board_size];
    
    if (queens==nullptr)
    {
        std::cout << "Could not allocate memory "<<std::endl;
        return 0;
    }
    // Initiate the queens array   
    else
        for (int i=0; i < board_size; i++)
            queens[i] = -1;
    
    //Solve the problem iteratively
    std::cout << "Solving iteratively... "<<std::endl;
    if (solve_n_queens_pr_iteratively(board_size,queens))
    {
        std::cout << "One solution is (queens are marked with 1): "<<std::endl;
        print_board(board_size, queens); 
    }
    else 
        std::cout << "No solution found "<<std::endl;   

    //Free memory  
    delete[] queens;
    return 0;
}